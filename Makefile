CC?=gcc

TALPO_MELT_MODE?=translatetomodule
MOD_SUFFIX=.so

INSTALL=install
PREFIX?=/usr/local/
DESTDIR?=
TARGET=$(DESTDIR)$(PREFIX)share/gcc-melt/talpo/

ifeq ($(TALPO_MELT_MODE), translatequickly)
  MOD_SUFFIX=.q.so
endif
ifeq ($(TALPO_MELT_MODE), translatedebug)
  MOD_SUFFIX=.n.so
endif

TALPO_TEST_SOURCES=$(wildcard talpo_test*.melt)
TALPO_TEST_SO=$(patsubst %.melt,%$(MOD_SUFFIX), $(TALPO_TEST_SOURCES))
TALPO_ARG_HANDLER_SOURCES=$(wildcard talpo_arg_*.melt)
TALPO_ARG_HANDLER_SO=$(patsubst %.melt,%$(MOD_SUFFIX), $(TALPO_ARG_HANDLER_SOURCES))
#file which can be compiled without dependancy (other files have to be manually
#set in the makefile
TALPO_NO_DEP_SOURCES=talpo_init_test.melt
TALPO_NO_DEP_SO=$(patsubst %.melt,%$(MOD_SUFFIX), $(TALPO_NO_DEP_SOURCES))

generalArgs=-fmelt-mode=$(TALPO_MELT_MODE)

all: talpo

init: 
	touch empty.c

install:
	$(RM) $(EMPTY)
	for f in $(wildcard *.melt) $(wildcard *.c); do				\
		$(INSTALL) -m644 -D $$f $(TARGETDIR)/melt-source/$$f;		\
	done
	for f in $(wildcard *.so); do						\
		$(INSTALL) -m755 -D $$f $(TARGETDIR)/melt-modules/$$f;		\
	done

talpo.modlis: $(TALPO_NO_DEP_SOURCES) $(TALPO_TEST_SOURCES) $(TALPO_ARG_HANDLER_SOURCES) talpo_dispatcher.melt
	rm -vf talpo.modlis
	echo $(subst .so,,$(subst .melt,$(MOD_SUFFIX),$^)) | sed 's/ /\n/g' >talpo.modlis

talpo_dispatcher$(MOD_SUFFIX).modlis: $(TALPO_NO_DEP_SOURCES) $(TALPO_TEST_SOURCES) $(TALPO_ARG_HANDLER_SOURCES)
	rm -vf talpo_dispatcher.modlis
	echo $(subst .so,,$(subst .melt,$(MOD_SUFFIX),$^)) | sed 's/ /\n/g' >talpo_dispatcher$(MOD_SUFFIX).modlis

$(TALPO_NO_DEP_SO) : $(TALPO_NO_DEP_SOURCES)
	$(CC) -fmelt-mode=$(TALPO_MELT_MODE) $(generalArgs) -fmelt-arg=$(subst $(MOD_SUFFIX),.melt,$@) -c empty.c

$(TALPO_TEST_SO) : $(TALPO_TEST_SOURCES) $(TALPO_NO_DEP_SO) 
	$(CC) -fmelt-mode=$(TALPO_MELT_MODE) $(generalArgs) -fmelt-arg=$(subst $(MOD_SUFFIX),.melt,$@) -fmelt-extra=talpo_init_test -fmelt-module-path=. -c empty.c

$(TALPO_ARG_HANDLER_SO) : $(TALPO_ARG_HANDLER_SOURCES) $(TALPO_NO_DEP_SO)
	$(CC) -fmelt-mode=$(TALPO_MELT_MODE) $(generalArgs) -fmelt-arg=$(subst $(MOD_SUFFIX),.melt,$@) -fmelt-extra=talpo_init_test -fmelt-module-path=. -c empty.c

talpo_dispatcher : $(TALPO_NO_DEP_SO) $(TALPO_TEST_SO) $(TALPO_ARG_HANDLER_SO) talpo_dispatcher.melt init talpo_dispatcher$(MOD_SUFFIX).modlis
	$(CC) -fmelt-mode=$(TALPO_MELT_MODE) $(generalArgs) -fmelt-arg=talpo_dispatcher.melt -fmelt-extra=@talpo_dispatcher$(MOD_SUFFIX) -fmelt-module-path=. -c empty.c

talpo: init talpo.modlis talpo_dispatcher

clean:
	rm -vf *.c
	rm -vf *.so
	rm -vf *\~
	rm -vf *.c%
	rm -vf *.modlis
