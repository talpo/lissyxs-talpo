;; -*- Lisp -*-
;; file talpo.melt
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment "***
    (C) Copyright 2011 Pierre Vittet
    This talpo_dispatcher.melt file is part of TALPO.

    TALPO is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3, or (at your option)
    any later version.

    TALPO is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TALPO; see the file COPYING3.  If not see
    <http://www.gnu.org/licenses/>.
***")

(comment "***
  The name of this MELT module is Talpo. This means mole (the animal) in
Esperanto (the international language). The idea below this name is that we can
travel through GCC as the mole burrow: there are quite blind but still can find
food (as we find usefull informations).

  The idea of Talpo is to offer to the user various way to insert tests on his
code into the GCC machinery. It can be done by inserting #pragma in the code,
explicitly give parameters, or through a file (we can easily insert new way to
insert test).

  For the moment test can only be inserted at GIMPLE state, but we could
imagine to enlarge this later. The main inconvenient is that each pass is
runned with a function scope (so we cannot do tests on a global scope).

  The kind of test that we can actually run is:
    testing that the result of a call to a function is tested to be (not) NULL.
    ...
  This is pretty easy to a new test, you will just need to do the following:
      -create an instance of class_talpo_test_model, giving some basic
        informations
      -writing the handler as a usual pass execution function
      -adding the test to the list lst_simple_test in the function
        talpo_init.

***")

;;init the list of tests + parse the different frontEnd.
(defun talpo_init ()
  ;;we set :container_value lst_registered_test as a list to recept tests
  (put_fields lst_registered_test :container_value (list) )
  (foreach_in_list
  ((get_field :container_value lst_frontEnd))
  (curPair curEl)
    ((get_field :talpo_frontEnd_closure curEl))
  )
)

;;True if there is at least one test registered.
(defun talpo_gate (pass)
  (if (is_non_empty_list (get_field :container_value lst_registered_test))
    (return pass)
  )
)

;;gate used by every new_test pass
;;Always true as if a pass has been registered, the user want to run it.
(defun generic_gate (pass)
  pass
)

;;this function positions the different pass after they have been registered.
(defun talpo_exec (pass)
  (debugtree "Entering talpo_exec" (null_tree))
  (debug_msg (get_field :container_value lst_registered_test)
              "Entering talpo_exec")
  (foreach_in_list
    ((get_field :container_value lst_registered_test))
    (curpair curTest)
    (debug_msg curTest "Debug on current test")
    (let ((curTest_mode (instance class_melt_mode
      :named_name (get_field :named_name curTest)
      :meltmode_help (get_field :talpo_test_model_help 
                        (get_field :talpo_test_model curTest))
      :meltmode_fun ((lambda (cmd moduldata)
        (let ((new_test
          (instance class_gcc_gimple_pass
            :named_name (get_field :named_name curTest)
            :gccpass_gate generic_gate
            :gccpass_exec (get_field :talpo_test_model_handler 
                            (get_field :talpo_test_model curTest))
            :gccpass_data curTest ;;the test itself
            :gccpass_properties_required ()
          )))
          (install_melt_gcc_pass new_test "after" "ssa" 0)
          (debug_msg new_test "new_test installed dynamically")
          (return new_test)
        )
      )
      ))))
    (install_melt_mode curTest_mode)
    (debug_msg curTest_mode "mode Installed"))
  )
)

(defun talpo_docmd (cmd moduldata)
    (let ((talpo
        (instance class_gcc_gimple_pass
            :named_name '"melt_talpo_pass"
            :gccpass_gate talpo_gate
            :gccpass_exec talpo_exec
            :gccpass_properties_required ()
          )
    ))
  (install_melt_gcc_pass talpo "after" "cfg" 0)
  (debug_msg talpo "talpo_mode installed talpo")
;; return the pass to accept the mode
  (return talpo)
))

;; MELT mode: precise the module
(definstance talpo
  class_melt_mode
  :named_name '"talpo"
  :meltmode_help '"Allows to ask run personnalized simple checks when compiling"
  :meltmode_fun talpo_docmd
)

;;initialise the list of the possible tests + parse the different frontEnds.
(talpo_init)

;;The goal of this function is to convert the function string names given by the
;;user into tree function declaration by parsing each function declaration now. 
;;This permit to use later tree comparison instead of string which should highly
;;improve performance.
(defun traverse_pre_gen (fndecl)
  (match (tree_content fndecl)
    (
      ?(tree_function_decl ?fndeclStr ?initialdcl)

        (foreach_in_list
        ((get_field :container_value lst_registered_test))
        (curPair curTest)
          (debugtree "testing traverse_pre_gen" (null_tree))
          (let ((mul_argstr (get_field :talpo_test_args curTest)))
            (foreach_in_multiple
              (mul_argstr)
              (curArg :long ix)
              (debugtree "testing traverse_pre_gen2" (null_tree))
              ;;(code_chunk str #{printf("comparing fndeclStr %s\n", $fndeclStr)}#)
              ;;(code_chunk str #{printf("comparing curarg%s\n",
              ;;                         melt_string_str($curArg))}#)
              (if (==s curArg (make_stringconst discr_string fndeclStr))
                  (let (( lst_args_trees (get_field :talpo_test_args_trees
                                          curTest)))
                    (list_append lst_args_trees fndecl)
                    (put_fields curTest :talpo_test_args_trees lst_args_trees)
                  )
              )
            )
          )
        )
    )
    (
      ?_(warningmsg_strv "Is not a valid function declaration"
           fndecl)
    )
  )
)

(register_pre_genericize_hook_first traverse_pre_gen)


(install_melt_mode talpo)
