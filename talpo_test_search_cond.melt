(comment "***
    (C) Copyright 2011 Pierre Vittet
    This talpo_test_search_cond.melt file is part of TALPO.

    TALPO is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3, or (at your option)
    any later version.

    TALPO is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TALPO; see the file COPYING3.  If not see
    <http://www.gnu.org/licenses/>.
***")

;;Register a test model that when we have a call to a function, the result is
;;tested.

;;as in case such as :
;;  myStr.file = fopen (...);
;;  if(myStr.file == NULL)...
;;
;;GCC generate some additionnal gimple_assign, such as :
;;  D.3286_4 = fopen (D.3285_3, D.3284_2);
;;  testStr.ptrfile = D.3286_4;
;;  D.3287_5 = testStr.ptrfile;
;;  if (D.3287_5 == 0B) 
;;
;;We can have the test in differents variables (here, D.3286 and D.3287 which
;;does not refer to the same variable), so here we declare a list of thoses
;;possible variables.
(definstance lst_possible_var class_container
  :container_value ((list))
)

;;This instance contain the function contening the test to the condtion.
;;it depend of each possible test on the condition (test that it is compared
;;to null, zero, negative...)
(definstance fun_testing_condition class_container
  :container_value ((lambda ()
                       (debug_msg () "no function testing condition inserted.")
                       (return)
                     )
                    )
)

;;Used to send a warning when we find that $strfunc is not properly tested.
(defun test_warn (strfunc :gimple g)
  (let ((strbuf (make_strbuf discr_strbuf)))
    (add2sbuf_strconst strbuf "Function '")
    (add2sbuf_string strbuf strfunc)
    (add2sbuf_strconst strbuf "' not followed by a test on his returned result.")
    (warning_at_gimple_strbuf g strbuf)
  )
)


;;detect the function contained in the string strfunc by using a match, return the lhs value
(defun test_detect_function (strfunc posArg :gimple g)
    (match g
      (
          ?(gimple_call  ?lhs 
                          ?(and ?callfndcl 
                          ?(tree_function_decl 
                          ?call_cstring
                          ?_))
                      ?nbargs
           )
          (if (==s (make_stringconst discr_string call_cstring) strfunc)
              ;;the returned tree will depend of the field
              ;;:talpo_test_args_position, 0 means we return lhs, 1, first
              ;;argument, 2, second argument...
              (if (or (null posArg) (==i 0 (get_int posArg)))
                (progn
                (if lhs
                  (return (make_tree discr_tree lhs))
                ;;Else there is no lhs part, so the function call is not
                ;;tested
                  (test_warn strfunc g)
                )
                )
              ;;else
                ;;we use posArg - 1 as gimple_call_nth_arg start at 0
                (let ((:tree nth_arg (gimple_call_nth_arg g (-i (get_int
                                                                 posArg) 1))))
                  (if nth_arg
                    (return make_tree discr_tree nth_arg)
                    (test_warn strfunc g)
                  )
                )
              )
          ;;else
            (return (make_tree discr_tree (null_tree)))
          )
      )
      (
          ?_((return (make_tree discr_tree (null_tree))))
      )
    )
)


;;This function is quite uneasy to understand.
;;It must determine if a gimple g is the last gimple of the curBB basic-block,
;;however we return true if it is not the last but is only followed by gimple
;;assignation over the matched gimple (which is quite frequent, as there can be
;;some generated variable switch after a gimple call.
  
;;to recap how it works:
;;  1) find g in the BB gimpleseq
;;  2) if g is the last stmt of BB, it returns true
;;  3) if g is followed by a gimple assignation matching a variable of g
;;      add it to lst_possible_var
;;      check if this is the last stmt and return true if it is the case
;;  4) else if it is not followed by an interesting gimple assignation, return
;;        false

(defun gimple_seq_last_stmt_with_assignation (curBB :gimple g)
  (let ((startSearch ()))
    (eachgimple_in_basicblock
    ((basicblock_content curBB))
    (:gimple curg)
      (if (==g curg g)
        (progn
          (setq startSearch :true)
          (if (==g (gimple_seq_last_stmt (gimple_seq_of_basic_block 
                                       (basicblock_content curBB))) curg)
            (return :true)
          )
        )
      ;;else
        (if startSearch
          (let ((lst (get_field :container_value lst_possible_var))
                (res_assign (is_corresponding_assign lst_possible_var curg))
               )
            (if res_assign
              (progn
              (list_append lst res_assign)
              (put_fields lst_possible_var :container_value lst)
              (if (==g (gimple_seq_last_stmt (gimple_seq_of_basic_block 
                                         (basicblock_content curBB))) curg)
                (return :true)
              )
              )
            ;;else
              (return)
            )
          )
        )
      )
    )
  )
)


;;check if the first gimple of the post dominator of $BB is a test checking that
;; $lhs_function is (not) NULL.
(defun post_dom_test_strfunc (lhs_function :basic_block bb)
  (let ((post_dom_bb (get_immediate_post_dominator (make_basicblock
                                                    discr_basic_block bb)))
        ;;(:gimple g (gimple_seq_first_stmt (gimple_seq_of_basic_block
        ;;                                   (basicblock_content post_dom_bb))))
        )
   (debugbasicblock "Entering post_dom_test_strfunc" (basicblock_content
                                                     post_dom_bb))
   (eachgimple_in_basicblock
     ((basicblock_content post_dom_bb))
     (:gimple g)
     (debuggimple "for each gimple in post_dom" g)
   ;;Pour chaque gimple du bb
    (let ((:long res_testfn (get_int (detect_function_is_tested () g))))
      (if (==i res_testfn 1)
        ;;call has been correctly matched
        (progn
          (debuggimple "call has been correctly matched" g)
          (return :true)
        )
      ;;else
        (if (==i res_testfn -1)
          (return)
        )
      )
    )
  ))
)

;;This function is called after we found a gimple_call corresponding to the one
;;we are looking for.
;;This function is called for the next gimple and must decide if the call is
;;correctly handle or not.
;;It can return 3 results under the form of a boxed integer:
  ;; 1 - gimple_call has been correctly handle
  ;; 0 - gimple_call must be handle in next gimple g
  ;; -1 - gimple_call has not been correctly handle
(defun detect_function_is_tested (unused :gimple g)
  (assert_msg "Trying fun_testing_condition but it is not a closure."
    (is_closure (get_field :container_value fun_testing_condition)))
  (if (tree_match_one_var (get_field :container_value lst_possible_var)
                          ((get_field :container_value fun_testing_condition) ()
                           g))
  ;;the call has been correctly matched
  (return (make_integerbox discr_integer 1))
  ;else
  (progn 
  ;;as in case such as :
  ;;  myStr.file = fopen (...);
  ;;  if(myStr.file == NULL)...
  ;;We generate some additionnal gimple_assign, we accept to have a
  ;;gimple_assign without considering that it is not immediatly
  ;;followed. Otherway we warn.
  (if (is_corresponding_assign lst_possible_var g)
    (progn
      (let ((lst (get_field :container_value lst_possible_var)))
        (list_append lst
          (is_corresponding_assign lst_possible_var g))
        (put_fields lst_possible_var :container_value lst)
      )
      ;;We must have a look at next gimple to decide
      (return (make_integerbox discr_integer 0))
    )
    ;;else
    (return (make_integerbox discr_integer -1))
   )
  )
  )
)


;;allow to detect untested call to the function identified by the string
;;(value) $STRFUNC
(defun test_detect_untested_function (strfunc posArg)
  (let ((:tree lhs_function (null_tree)) 
       (state ())
     )
    (each_bb_current_fun
    ()
    (:basic_block bb)
    (eachgimple_in_basicblock 
      (bb)
      (:gimple g)
      (debuggimple "for each gimple" g)
      (if state;;if previous gimple was the searched function
        (let ((:long res_detect (get_int (detect_function_is_tested () g))))
          (if (==i res_detect 1)
            ;;gimple call has been correctly handle.
              (progn 
                (setq state ())
                (put_fields lst_possible_var :container_value (list))
              )
            ;;else
            (if (==i res_detect -1) 
              (progn
                (test_warn strfunc g)
                (put_fields lst_possible_var :container_value (list))
                (setq state ())
              )
            ;;else
              ()
            )
          )
        )
      )
      (if (null state)
        (progn
          (setq lhs_function (tree_content (test_detect_function strfunc
                                            posArg g)))
          (if lhs_function
            ;;If the searched function, is the last element of a basic bloc, and
            ;;if the first gimple of the first postdominator of the bb is the
            ;;test.
            (progn
            (put_fields lst_possible_var :container_value 
                        (list (make_tree discr_tree lhs_function)))

            (if (gimple_seq_last_stmt_with_assignation
                      (make_basicblock discr_basic_block bb) g)
              (if (post_dom_test_strfunc (make_tree discr_tree lhs_function) bb
                   )
                ()
                (test_warn strfunc g)
              )
            ;;Else gimple is not the last statement of seq and we check next
            ;; gimple.
              (setq state :true)
            ))
          )
        )
      )
    ))
  )
)

;;detect a gimple cond with the null pointer
;;the cond can be of type == or !=
;;it return the lhs part of the cond (or null if no match)
(defun test_detect_cond_with_null (useless :gimple g )
    (match g
        (
              ?(gimple_cond_notequal ?lhs
                                     ?(and ?rhs
                                     ?(tree_integer_cst
                                     0))
               )(
                    (return (make_tree discr_tree lhs))
               )
          )
          (
            ?(gimple_cond_equal ?lhs
                                   ?(and ?rhs
                                   ?(tree_integer_cst
                                   0))
             )(
                    (return (make_tree discr_tree lhs))
             )
          )
          (
            ?_((return (make_tree discr_tree (null_tree))))
          )
      )
)

(defun test_null_handler(pass)
  (debug_msg pass "test_null_handler pass at start")
  (assert_msg "test_null_handler check cfun has cfg" (cfun_has_cfg))
  (debugtree "test_null_handler start cfundecl" (cfun_decl))
  (debug_msg (get_field :gccpass_data pass) "data of the test_null_handler")
  ;;this test has just one arg in the list
  (debug_msg (pair_head (list_first (get_field :gccpass_data pass)))
                          "data of the test_null_handler")
  (let ((curTest (get_field :gccpass_data pass))
        (TestArg (get_field :talpo_test_args curTest))
        (TestPosArg (get_field :talpo_test_args_position curTest))
       )
    ;;insert the function testing the condition
    (put_fields fun_testing_condition :container_value
     test_detect_cond_with_null)
    ;;this test has just one arg in the list
    (test_detect_untested_function (multiple_nth TestArg 0)
      (multiple_nth TestPosArg 0))
    (debug_msg pass "test_null_handler at end")
  )
)

(definstance iTest_Null class_talpo_test_model
  :named_name '"testNull"
  :talpo_test_model_handler test_null_handler
  :talpo_test_model_help '"Check that a call to a given function is tested to be (not) null."
  :talpo_test_model_nbargs '1
)

(defun test_detect_cond_with_zero (useless :gimple g )
    (debuggimple "Entering test_detect_cond_with_zero" g)
    (match g
        (
              ?(gimple_cond_notequal ?lhs
                                     ?(and ?rhs
                                     ?(tree_integer_cst
                                     0))
               )(
                    (return (make_tree discr_tree lhs))
               )
          )
          (
            ?(gimple_cond_equal ?lhs
                                   ?(and ?rhs
                                   ?(tree_integer_cst
                                   0))
             )(
                    (return (make_tree discr_tree lhs))
             )
          )
          (
            ?_((return (make_tree discr_tree (null_tree))))
          )
      )
)

(defun test_zero_handler(pass)
  (debug_msg pass "test_zero_handler pass at start")
  (assert_msg "test_zero_handler check cfun has cfg" (cfun_has_cfg))
  (debugtree "test_zero_handler start cfundecl" (cfun_decl))
  (debug_msg (get_field :gccpass_data pass) "data of the test_zero_handler")
  ;;this test has just one arg in the list
  (debug_msg (pair_head (list_first (get_field :gccpass_data pass)))
                          "data of the test_zero_handler")
  (let ((curTest (get_field :gccpass_data pass))
        (TestArg (get_field :talpo_test_args curTest))
        (TestPosArg (get_field :talpo_test_args_position curTest))
       )
    ;;insert the function testing the condition
    (put_fields fun_testing_condition :container_value
     test_detect_cond_with_zero)
    ;;this test has just one arg in the list
    (test_detect_untested_function (multiple_nth TestArg 0)
      (multiple_nth TestPosArg 0))
    (debug_msg pass "test_zero_handler at end")
  )
)

(definstance iTest_Zero class_talpo_test_model
  :named_name '"testZero"
  :talpo_test_model_handler test_zero_handler
  :talpo_test_model_help '"Check that a call to a given function is tested to be (not) zero."
  :talpo_test_model_nbargs '1
)

(defun test_detect_cond_with_neg (useless :gimple g )
    (debuggimple "Entering test_detect_cond_with_neg" g)
    (match g
        (
              ?(gimple_cond_less ?lhs
                                     ?(and ?rhs
                                     ?(tree_integer_cst
                                     0))
               )(
                    (progn
                    (debuggimple "finding gimple cond" g)
                    (return (make_tree discr_tree lhs))

                    )
               )
          )
          (
            ?(gimple_cond_greater_or_equal ?lhs
                                           ?(and ?rhs
                                           ?(tree_integer_cst 0))
             )(
                    (return (make_tree discr_tree lhs))
             )
          )
          (
            ?_((return (make_tree discr_tree (null_tree))))
          )
      )
)

(defun test_neg_handler(pass)
  (debug_msg pass "test_neg_handler pass at start")
  (assert_msg "test_neg_handler check cfun has cfg" (cfun_has_cfg))
  (debugtree "test_neg_handler start cfundecl" (cfun_decl))
  (debug_msg (get_field :gccpass_data pass) "data of the test_neg_handler")
  ;;this test has just one arg in the list
  (debug_msg (pair_head (list_first (get_field :gccpass_data pass)))
                          "data of the test_neg_handler")
  (let ((curTest (get_field :gccpass_data pass))
        (TestArg (get_field :talpo_test_args curTest))
        (TestPosArg (get_field :talpo_test_args_position curTest))
       )
    ;;insert the function testing the condition
    (put_fields fun_testing_condition :container_value
     test_detect_cond_with_neg)
    ;;this test has just one arg in the list
    (test_detect_untested_function (multiple_nth TestArg 0)
      (multiple_nth TestPosArg 0))
    (debug_msg pass "test_neg_handler at end")
  )
)

(definstance iTest_Neg class_talpo_test_model
  :named_name '"testNeg"
  :talpo_test_model_handler test_neg_handler
  :talpo_test_model_help '"Check that a call to a given function is tested to be (not) neg."
  :talpo_test_model_nbargs '1
)

(insert_test_model iTest_Null)
(insert_test_model iTest_Zero)
(insert_test_model iTest_Neg)

