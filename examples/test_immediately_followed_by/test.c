#include <unistd.h>
#include <stdlib.h>

int basic_no_warn()
{
  char * curDir = (char *) malloc (sizeof(char) *3);
  curDir = ".";
  chroot(curDir);
  chdir(curDir);
  return 0;
}

int basic_warn_once()
{
  int i = 0;
  char * curDir = (char *) malloc (sizeof(char) *3);
  curDir = ".";
  chroot(curDir);
  i++;
  chdir(curDir);
  return 0;
}

int not_same_var_warn_once()
{
  char * curDir = (char *) malloc (sizeof(char) *3);
  char * notCurDir = (char *) malloc (sizeof(char) *3);
  curDir = ".";
  chroot(curDir);
  chdir(notCurDir);
  return 0;
}

int cond_no_warn()
{
  int i = 0;
  char * curDir = (char *) malloc (sizeof(char) *3);
  curDir = ".";
  if(i== 1)
    chroot(curDir);
  chdir(curDir);
  return 0;
}

typedef struct _myStr
{
  char * stringVar;
}myStr;

int use_struct_no_warn (void){
  myStr testStr;
  testStr.stringVar = (char *) malloc (sizeof(char)*3);
  testStr.stringVar = ".";
  chroot (testStr.stringVar);
  chdir (testStr.stringVar);
  free (testStr.stringVar);
  return 1;
}

int use_struct__warn_once (void){
  int i=0;
  myStr testStr;
  testStr.stringVar = (char *) malloc (sizeof(char)*3);
  testStr.stringVar = ".";
  chroot (testStr.stringVar);
  i++;
  chdir (testStr.stringVar);
  free (testStr.stringVar);
  return 1;
}

int main()
{
  basic_no_warn();
  basic_warn_once();
  not_same_var_warn_once();
  cond_no_warn();
  use_struct_no_warn();
  use_struct__warn_once();
  return 0;
}
