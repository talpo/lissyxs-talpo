
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Test for GCC > 4.6.0 */
#if __GNUC__ > 4 || \
(__GNUC__ == 4 && (__GNUC_MINOR__ > 6 ))
#pragma MELT Talpo testnull(fopen)
#else
#pragma GCCPLUGIN melt testnull(fopen)
#endif



int
basic_no_warn(void)
{
  FILE * test;
  test = fopen("toto","a");
  if (test == NULL)
    return 1;
  return 0;
}


int
basic2_no_warn(void)
{
  FILE * test;
  test = fopen("toto","a");
  if (test)
    return 1;
  return 0;
}


int
basic3_no_warn(void)
{
  FILE * test;
  test = fopen("toto","a");
  if (!test)
    return 1;
  return 0;
}

FILE *
basic_warn_once(void)
{
  FILE * test;
  test = fopen("toto","a");
  return test;
}


int
cond_no_warn(void)
{
  int i=0;
  FILE * test;
  if( i == 1)
    test = fopen("toto","a");
  else
    test = fopen("tata", "a");
  if (test == 0)
    return 1;
  return 0;
}


FILE *
cond_warn_once(void)
{
  int i=0;
  FILE * test;
  FILE * test2;
  if( i == 1)
    test = fopen("toto","a");
  else
    test2 = fopen("tata", "a");
  if (test == 0)
    return test2;
  return test2;
}


int
loop_warn_once(void)
{
  int i=0;
  FILE * test;
  while ( i == 0){
    i++;
    test=fopen("toto","a");
  }
  if (test == NULL)
    return 1;
  return 0;
}


int
loop_no_warn(void)
{
  int i=0;
  FILE * test;
  while ( i == 0){
    i++;
    test=fopen("toto","a");
    if (test == NULL)
      return 1;
  }
  return 0;
}

typedef struct _myStr
{
  FILE * ptrfile;
}myStr;


int
use_struct_no_warn (void)
{
  myStr * testStr = (myStr *) malloc(sizeof(myStr));
  int i=1;
  if(i<1)
    testStr->ptrfile=fopen("toto", "a");
  if(!testStr->ptrfile){
    return 0;
  }
  return 1;
}


int
use_struct_warn_once (void)
{
  myStr * testStr = (myStr *) malloc(sizeof(myStr));
  int i=1;
  if(i<1)
    testStr->ptrfile=fopen("toto", "a");
  i++;
  if(!testStr->ptrfile){
    return 0;
  }
  return 1;
}


int
main(void)
{
  basic_no_warn();
  basic2_no_warn();
  basic3_no_warn();
  basic_warn_once();
  cond_no_warn();
  cond_warn_once();
  loop_warn_once();
  loop_no_warn();
  use_struct_no_warn();
  use_struct_warn_once();
  return 0;
}
