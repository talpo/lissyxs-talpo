#include <unistd.h>
#include <stdlib.h>
/* Test for GCC > 4.6.0 */
#if __GNUC__ > 4 || \
(__GNUC__ == 4 && (__GNUC_MINOR__ > 6 ))
#pragma MELT Talpo testFollowedBy(chroot, 1, chdir, 1)
#else
#pragma GCCPLUGIN melt testFollowedBy(chroot, 1, chdir, 1)
#endif




int
basic_no_warn()
{
  int i=0;
  char * curDir = (char *) malloc (sizeof(char) *3);
  curDir = ".";
  chroot(curDir);
  i++;
  chdir(curDir);
  free(curDir);
  return i;  
}


int
basic_warn_once()
{
  int i=0;
  char * curDir = (char *) malloc (sizeof(char) *3);
  curDir = ".";
  chroot(curDir);
  i++;
  free(curDir);
  return i;  
}


int
not_same_var_warn_once()
{
  char * curDir = (char *) malloc (sizeof(char) *3);
  char * notCurDir = (char *) malloc (sizeof(char) *3);
  curDir= ".";
  notCurDir= ".";
  chroot(curDir);
  chdir(notCurDir);
  free(curDir);
  free(notCurDir);
  return 0;
}


int
cond_warn_once()
{
  int i=1;
  char * curDir = (char *) malloc (sizeof(char) *3);
  curDir= ".";
  chroot(curDir);
  if (i!=1)
    chdir(curDir);
  free(curDir);
  return 0;
}


int
cond_no_warn()
{
  int i=1;
  char * curDir = (char *) malloc (sizeof(char) *3);
  curDir= ".";
  if (i == 1)
    chroot(curDir);
  chdir(curDir);
  free(curDir);
  return 0;
}


int
main()
{
  basic_no_warn();
  basic_warn_once();
  not_same_var_warn_once();
  cond_warn_once();
  cond_no_warn();
  return 0;
}
